Install dependencies:
cd client && npm run install
cd server1 && npm run install
cd server2 && npm run install

Run:
docker-compose -f docker-compose.yml up --build

Cleanup:
docker-compose -f docker-compose.yml down
