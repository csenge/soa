import React, { Component } from 'react';
import AnimalShelters from './components/AnimalShelters';

import './App.css';

class App extends Component {
  render() {
    return (
      <div className="App">
				<AnimalShelters />
      </div>
    );
  }
}

export default App;
