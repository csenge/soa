import * as React from "react";
import axios from "axios";

export default class AnimalShelters extends React.Component{
	state = {
		selectedShelterId: null,
		animalShelters: null,
		pets: null,
	};

	getAnimalShelters() {
		axios.get('http://localhost:3002/animalShelters').then((response) => {
			const responseData = response.data;

			if (responseData.success) {
				this.setState({
					animalShelters: responseData.data,
				});
			}
		});
	}

	loadPets(shelterId) {
		this.setState({
			selectedShelterId: shelterId,
		});

		axios.get(`http://localhost:3002/animalShelters/${shelterId}/pets`).then((response) => {
			const responseData = response.data;

			if (responseData.success) {
				this.setState({
					pets: responseData.data,
				});
			}
		});
	}

	goBack() {
		this.setState({
			pets: null,
			selectedShelterId: null,
		});
	}

	renderAnimalShelterCard(animalShelter, size) {
		return (
			<div className={`card col-${size}`}>
				<img className="card-img-top" src={ animalShelter.picture } alt="Card image cap" />
				<div className="card-body">
					<h5 className="card-title">{ animalShelter.name }</h5>
					<h6 className="card-subtitle">{ animalShelter.location }</h6>
					<p className="card-text">{ animalShelter.description }</p>
					{
						size === '6' ? (
							<a href="#" onClick={ () => this.loadPets(animalShelter.id) } className="btn btn-primary">See pets</a>
						) : (
							<a href="#" onClick={ () => this.goBack() } className="btn btn-primary">Exit</a>
						)
					}
				</div>
			</div>
		);
	}

	renderAnimalShelters() {
		if (!this.state.animalShelters) {
			this.getAnimalShelters();

			return <div className="text-center">Loading</div>;
		}

		return this.state.animalShelters.map((animalShelter) => this.renderAnimalShelterCard(animalShelter, '6'));
	}

	adoptPet(shelterId, petId) {
		axios.patch(`http://localhost:3002/animalShelters/${shelterId}/pets/${petId}`, {
			isAdopted: true,
		}).then(() => {
			this.loadPets(shelterId);
		});
	}

	renderAnimalsList() {
		const {
			selectedShelterId,
			animalShelters,
			pets,
		} = this.state;

		const selectedShelter = this.renderAnimalShelterCard(animalShelters.find((animalShelter) => animalShelter.id === selectedShelterId), '12');

		return (
			<div>
				{ selectedShelter }
				<hr />
				<div className="row">
					{ !!pets && pets.map(pet => (
						<div className={`card col-4`}>
							<img className="card-img-top" src={ pet.details[0].picture } alt="Card image cap" />
							<div className="card-body">
								<ul className="list-group list-group-flush">
									<li className="list-group-item">{ pet.name }</li>
									<li className="list-group-item">{ pet.details[0].petBreed }</li>
									<li className="list-group-item">{ pet.age } years old</li>
								</ul>
								<br />
								<br />
								{
									!pet.isAdopted && (
										<button onClick={ () => this.adoptPet(pet.animalShelterId, pet.id) } className="btn btn-primary">Get home</button>
									) || (
										<button className="btn btn-secondary" disabled>Get home</button>
									)

								}
							</div>
						</div>
					)) }
				</div>
			</div>
		);
	}

	render () {
		const { selectedShelterId } = this.state;

		return (
			<div className="animal-shelters-list">
				<div className="container">
					<div className="row">
						{ !selectedShelterId && this.renderAnimalShelters() }
						{ !!selectedShelterId && this.renderAnimalsList() }
					</div>
				</div>
			</div>
		);
	}
}
