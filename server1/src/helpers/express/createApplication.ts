import * as express from "express";
import * as bodyParser from "body-parser";
import handleErrors from "../../middlewares/handleErrors";
import * as cors from "cors";

export default function(routers: any): express.Application {
	let app = express();

	app.use(bodyParser.urlencoded({ extended: false }));
	app.use(bodyParser.json());

	app.use(cors());

	app = routers(app);

	app.use(handleErrors);

	return app;
}
