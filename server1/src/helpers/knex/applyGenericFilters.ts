import * as Knex from "knex";
import {
	GenericFilters,
	GenericOrder,
	GenericPagination,
	GenericFilter,
} from "../../typesX";

const applyPaginationRules = (
	query: Knex.QueryBuilder,
	paginationRules: GenericPagination,
): Knex.QueryBuilder => {
	const {
		limit,
		offset,
	} = paginationRules;

	if (limit) {
		query = query.limit(limit);
	}

	if (offset) {
		query = query.offset(offset);
	}

	return query;
};

const applyFilterRules = (
	groupQuery: Knex.QueryBuilder,
	filterRules: GenericFilters,
): Knex.QueryBuilder => {
	groupQuery = groupQuery.orWhere((query: Knex.QueryBuilder) => {
		filterRules.forEach((filterRule: GenericFilter | GenericFilters) => {
			if (filterRule.length) {
				query = applyFilterRules(query, filterRule);
			} else {
				const {
					property,
					value,
					op,
				} = filterRule;

				if (!op) {
					query = query.where(property, value);
				} else {
					switch (op) {
						case "IN":
							query = query.whereIn(property, value);
							break;
						case "NOT_IN":
							query = query.whereNotIn(property, value);
							break;
						case "NULL":
							query = query.whereNull(property);
							break;
						case "LIKE":
							query = query.where(property, "like", value);
							break;
						case "BETWEEN":
							query = query.whereBetween(property, value);
							break;
						case "NOT_BETWEEN":
							query = query.whereNotBetween(property, value);
							break;
						default:
							query = query.where(property, op, value);
							break;
					}
				}
			}
		});

		return query;
	});

	return groupQuery;
};

const applyOrderRules = (
	query: Knex.QueryBuilder,
	orderRules: GenericOrder,
): Knex.QueryBuilder => {
	Object.keys(orderRules).forEach((orderRuleName: string) => {
		query = query.orderBy(orderRuleName, orderRules[orderRuleName]);
	});

	return query;
};

export const applyGenericFilters = (
	query: Knex.QueryBuilder,
	paginationRules?: GenericPagination,
	filterRules?: GenericFilters,
	orderRules?: GenericOrder,
): Knex.QueryBuilder => {
	if (paginationRules) {
		query = applyPaginationRules(query, paginationRules);
	}

	if (filterRules) {
		query = applyFilterRules(query, filterRules);
	}

	if (orderRules) {
		query = applyOrderRules(query, orderRules);
	}

	return query;
};
