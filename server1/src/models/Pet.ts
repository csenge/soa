import {
	Allow,
	MinLength,
} from "class-validator";
import {
	ClassValidatorDefinition,
	ClassValidator,
} from "../helpers/classValidator/ClassValidatorModel";

export interface PetDefinition extends ClassValidatorDefinition {
	id?: string | number;
	petType: string | number;
	petBreed: string;
	picture: string;
}

export class PetModel extends ClassValidator {
	@Allow()
	id?: string | number;

	@MinLength(1)
	petType: string | number;

	@MinLength(1)
	petBreed: string;

	@MinLength(1)
	picture: string;

	constructor(pet: PetDefinition) {
		super();

		this.id = pet.id;
		this.petType = pet.petType;
		this.petBreed = pet.petBreed;
		this.picture = pet.picture;
	}
}
