import * as PetsDao from "../dao/PetsDao";
import { PetDefinition, PetModel } from "../models/Pet";
import ErrorHandler from '../utils/ErrorHandler';
import {
	GenericFilters,
	GenericOrder,
	GenericPagination,
} from "../typesX";

export const getAll = async () => {
	return await PetsDao.getAll();
};

export const getFilteredData = async (
	paginationRules?: GenericPagination,
	filterRules?: GenericFilters,
	orderRules?: GenericOrder,
) => {
	return await PetsDao.getFiltered(paginationRules, filterRules, orderRules);
};

export const getById = async (id: PetDefinition["id"]) => {
	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_PROVIDED");
	}

	const pet = await PetsDao.getById(id);

	if (!pet) {
		throw ErrorHandler.notFound("PET_NOT_FOUND");
	}

	return pet;
};

export const add = async (petData: PetDefinition) => {
	const pet = new PetModel(petData);

	const errors = await pet.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const createdUser = PetsDao.add(pet);

	if (!createdUser) {
		throw ErrorHandler.badImplementation("PET_CANNOT_CREATE");
	}

	return createdUser;
};

export const remove = async (id: PetDefinition["id"]) => {
	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_PROVIDED");
	}

	const removed = await PetsDao.remove(id);

	if (!removed) {
		throw ErrorHandler.badImplementation("PET_CANNOT_REMOVE");
	}

	return {
		removed,
	};
};

export const update = async (id: PetDefinition["id"], petData: PetDefinition) => {
	const pet = new PetModel(petData);

	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_FOUND");
	}

	const errors = await pet.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const updatedUser = await PetsDao.update(id, pet);

	if (!updatedUser) {
		throw ErrorHandler.badImplementation("PET_CANNOT_UPDATE");
	}

	return updatedUser;
};

export const patch = async (id: PetDefinition["id"], data: any) => {
	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_FOUND");
	}

	const oldUser: any = await getById(id);
	const newUser = new PetModel(Object.assign({}, oldUser, data));

	const errors = await newUser.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const updatedUser = await PetsDao.update(id, newUser);

	if (!updatedUser) {
		throw ErrorHandler.badImplementation("PET_CANNOT_UPDATE");
	}

	return updatedUser;
};
