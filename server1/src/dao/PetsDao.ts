import Knex from "knex";
import { PetDefinition, PetModel } from "../models/Pet";
import modelNormalizer from "../utils/modelNormalizer";
import { applyGenericFilters } from "../helpers/knex/applyGenericFilters";
import { mysqlDbExecute as dbExecute } from "../helpers/knex/client";
import {
	GenericFilters,
	GenericOrder,
	GenericPagination,
} from "../typesX";

const TABLE_NAME = "pets";
const DB_SECRETS_PREFIX = "MYSQL_DB";

export const getAll = async () => {
	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		const pets = await trx(TABLE_NAME).select();

		return modelNormalizer(pets, PetModel);
	});
};

export const getById = async (id: PetDefinition["id"]) => {
	if (!id) {
		return null;
	}

	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		const pets = await trx(TABLE_NAME).select().where("id", id);

		if (!pets || !pets.length) {
			return null;
		}

		return modelNormalizer(pets, PetModel);
	});
};

export const getFiltered = async (
	paginationRules?: GenericPagination,
	filterRules?: GenericFilters,
	orderRules?: GenericOrder,
) => {
	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		let query = trx(TABLE_NAME);

		if (filterRules || orderRules || paginationRules) {
			query = applyGenericFilters(query, paginationRules, filterRules, orderRules);
		}

		const pets = await query.select();

		return modelNormalizer(pets, PetModel);
	});
};

export const add = async (pet: PetDefinition) => {
	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		const result = await trx(TABLE_NAME).insert(pet);

		if (!result || result.length !== 1) {
			return null;
		}

		return result[0];
	}).then(async (id: any) => {
		if (!id) {
			const documents: any = await getById(id);

			return documents[0];
		}
	});
};

export const remove = async (id: PetDefinition["id"]) => {
	if (!id) {
		return null;
	}

	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		return await trx(TABLE_NAME).delete().where("id", id);
	}).then((removed: any) => {
		return !!removed;
	});
};

export const update = async (id: PetDefinition["id"], pet: PetDefinition) => {
	if (!id) {
		return null;
	}

	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		return await trx(TABLE_NAME).update(pet).where("id", id);
	}).then(async () => await getById(id));
};
