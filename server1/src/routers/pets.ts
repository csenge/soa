import express from "express";
import * as PetsController from "../controllers/PetsController";
import handleAsyncRoutes from "../middlewares/handleAsyncRoutes";

export const handler = function(app: express.Application): express.Application  {
	app.get("/pets", handleAsyncRoutes(async () => {
		return await PetsController.getAll();
	}));

	app.get("/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				id,
			},
		} = req;

		return await PetsController.getById(id);
	}));

	app.post("/pets/filter", handleAsyncRoutes(async (req: express.Request) => {
		const {
			body: {
				pagination,
				filters,
				sorting,
			},
		} = req;

		return await PetsController.getFilteredData(
			pagination,
			filters,
			sorting,
		);
	}));

	app.post("/pets", handleAsyncRoutes(async (req: express.Request) => {
		const { body } = req;

		return await PetsController.add(body);
	}));

	app.put("/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params,
			body,
		} = req;

		return await PetsController.update(params.id, body);
	}));

	app.patch("/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params,
			body,
		} = req;

		return await PetsController.patch(params.id, body);
	}));

	app.delete("/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				id,
			},
		} = req;

		return await PetsController.remove(id);
	}));

	return app;
};
