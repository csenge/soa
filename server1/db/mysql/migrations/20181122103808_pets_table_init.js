
exports.up = function(knex, Promise) {
	return knex.schema.createTable("pets", table => {
		table.engine("InnoDB");
		table.charset("utf8mb4");
		table.collate("utf8mb4_unicode_ci");

		table.bigIncrements();
		table.timestamps(true, true);

		table.string("petType").notNullable();
		table.string("petBreed").notNullable();
		table.string("picture").notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("pets");
};
