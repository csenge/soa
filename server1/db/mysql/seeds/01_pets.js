exports.seed = function(knex, Promise) {
  return knex('pets').del()
    .then(function () {
      return knex('pets').insert([
        {
	        petType: "dog",
	        petBreed: "Ciobanesc German",
	        picture: "https://www.animalzoo.ro/wp-content/uploads/2013/04/www.vonpetworld.com_-650x487.jpg",
        },
	      {
		      petType: "dog",
		      petBreed: "Labrador Retriever",
		      picture: "https://embarkvet.com/wp-content/uploads/2018/07/LabradorRetriever_dogdnatest.jpg",
	      },
	      {
		      petType: "dog",
		      petBreed: "Pudel",
		      picture: "https://www.purina.ro/sites/g/files/mcldtz1521/files/2018-04/d14_0.jpg",
	      },
	      {
		      petType: "dog",
		      petBreed: "Beagle",
		      picture: "http://www.dogzone.com/images/breeds/beagle.jpg",
	      },
	      {
		      petType: "dog",
		      petBreed: "Chihuahua",
		      picture: "https://www.wowwow.ro/cache/4/d/8/9/c/4d89cf52fd441119798c6b61e37da5e1fbb1b5d0.jpeg",
	      },
      ]);
    });
};
