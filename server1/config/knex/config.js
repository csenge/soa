const yaml = require("js-yaml");
const fs = require("fs");

module.exports = ({
	stage,
	clientType,
	dbIdentifierPrefix,
}) => {
	const secrets = yaml.safeLoad(fs.readFileSync(`../../secrets/secrets.${stage}.yml`, 'utf8'));

	return {
		client: clientType,
		connection: {
			database: secrets[`${dbIdentifierPrefix}_NAME`],
			host: secrets[`${dbIdentifierPrefix}_HOST`],
			user: secrets[`${dbIdentifierPrefix}_USER`],
			password: secrets[`${dbIdentifierPrefix}_PASSWORD`],
			timezone: "UTC",
		},
		pool: {
			min: 0,
			max: 10,
		},
		migrations: {
			directory: "../../db/mysql/migrations",
			tableName: 'knex_migrations',
		},
		seeds: {
			directory: "../../db/mysql/seeds",
		},
	};
};