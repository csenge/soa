import express from "express";
import * as yaml from "js-yaml";
import * as fs from "fs";
import * as cluster from "cluster";
import * as os from "os";

const [, , stage, port] = process.argv;
const secrets = yaml.safeLoad(fs.readFileSync(`./secrets/secrets.${stage}.yml`, 'utf8'));
process.env = Object.assign({}, process.env, secrets);

import createApplication from "./src/helpers/express/createApplication";
import { handler as animalSheltersHandler } from "./src/routers/animalShelters";
import { handler as animalShelterPetsHandler } from "./src/routers/animalSheltersPets";

if (cluster.isMaster) {
	const cpuCount = os.cpus().length;

	for (let i = 0; i < cpuCount; i += 1) {
		cluster.fork();
	}

	cluster.on('exit', cluster.fork);
} else {
	const app = createApplication((app: express.Application) => {
		animalSheltersHandler(app);
		animalShelterPetsHandler(app);

		return app;
	});

	app.listen(port, () => console.log(`App listen on port ${port}!`));
}
