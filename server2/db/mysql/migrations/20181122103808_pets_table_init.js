
exports.up = function(knex, Promise) {
	return knex.schema.createTable("pets", table => {
		table.engine("InnoDB");
		table.charset("utf8mb4");
		table.collate("utf8mb4_unicode_ci");

		table.bigIncrements();
		table.timestamps(true, true);

		table.integer("petTypeId").notNullable();
		table.integer("animalShelterId").notNullable();
		table.string("name").notNullable();
		table.float("age").notNullable();
		table.boolean("isAdopted").notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("pets");
};
