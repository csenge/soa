
exports.up = function(knex, Promise) {
	return knex.schema.createTable("animal_shelters", table => {
		table.engine("InnoDB");
		table.charset("utf8mb4");
		table.collate("utf8mb4_unicode_ci");

		table.bigIncrements();
		table.timestamps(true, true);

		table.string("name").notNullable();
		table.string("location").notNullable();
		table.string("description").notNullable();
		table.string("picture").notNullable();
	});
};

exports.down = function(knex, Promise) {
	return knex.schema.dropTable("animal_shelters");
};
