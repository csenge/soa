exports.seed = function(knex, Promise) {
  return knex('pets').del()
    .then(function () {
      return knex('pets').insert([
        {
	        petTypeId: 1,
	        animalShelterId: 1,
	        name: "Bagel",
	        age: 1,
	        isAdopted: false,
        },
	      {
		      petTypeId: 2,
		      animalShelterId: 1,
		      name: "Birdie",
		      age: 0.4,
		      isAdopted: false,
	      },
	      {
		      petTypeId: 3,
		      animalShelterId: 1,
		      name: "Daisy",
		      age: 5,
		      isAdopted: true,
	      },
	      {
		      petTypeId: 4,
		      animalShelterId: 1,
		      name: "Gingi",
		      age: 3,
		      isAdopted: false,
	      },
	      {
		      petTypeId: 2,
		      animalShelterId: 1,
		      name: "Jasmine",
		      age: 3,
		      isAdopted: true,
	      },
	      {
		      petTypeId: 5,
		      animalShelterId: 1,
		      name: "Margo",
		      age: 6,
		      isAdopted: false,
	      },

	      {
		      petTypeId: 3,
		      animalShelterId: 2,
		      name: "Marshmellow",
		      age: 5,
		      isAdopted: false,
	      },
	      {
		      petTypeId: 1,
		      animalShelterId: 2,
		      name: "Midnight",
		      age: 12,
		      isAdopted: true,
	      },
	      {
		      petTypeId: 4,
		      animalShelterId: 2,
		      name: "Paddington",
		      age: 1,
		      isAdopted: false,
	      },
      ]);
    });
};
