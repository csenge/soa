exports.seed = function(knex, Promise) {
  return knex('animal_shelters').del()
    .then(function () {
      return knex('animal_shelters').insert([
        {
	        name: "Pet joy",
	        location: "Cluj Napoca",
	        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. ",
	        picture: "https://evz.ro/image-605-388/2011-04/adapostcaini1.jpg"
        },
	      {
		      name: "Centrul de ecarisaj",
		      location: "Cluj Napoca",
		      description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
		      picture: "https://ziaruldegarda.ro/wp-content/uploads/2018/02/Public-Shelter-Bacau-3.jpg"
	      },
      ]);
    });
};
