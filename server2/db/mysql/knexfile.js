const commonKnexConfig = require ("../../config/knex/config");

const CLIENT_TYPE = 'mysql';
const DB_IDENTIFIER_PREFIX = 'MYSQL_DB';

module.exports = {
	get docker() {
		return commonKnexConfig({
			stage: 'docker',
			clientType: CLIENT_TYPE,
			dbIdentifierPrefix: DB_IDENTIFIER_PREFIX,
		});
	},

	get local() {
		return commonKnexConfig({
			stage: 'local',
			clientType: CLIENT_TYPE,
			dbIdentifierPrefix: DB_IDENTIFIER_PREFIX,
		});
	},
};
