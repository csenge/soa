const path = require("path");
const nodeExternals = require('webpack-node-externals');

module.exports = {
	target: "node",
	externals: [nodeExternals()],
	entry: {
		expressServer: "./express.ts",
	},
	resolve: {
		extensions: [".js", ".json", ".ts", ".tsx"],
	},
	output: {
		libraryTarget: "commonjs",
		path: path.join(__dirname, "../../.webpack"),
		filename: "[name].js",
	},
	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				use: [
					{
						loader: "ts-loader"
					}
				]
			},
			{
				test: /\.js$/,
				loaders: [ "babel-loader" ],
				exclude: /node_modules/,
			},
			{
				test: /\.json$/,
				loader: "json-loader",
			},
		],
	},
};
