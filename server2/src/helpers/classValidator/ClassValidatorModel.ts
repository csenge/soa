import {
	Allow,
	validate as doValidate,
} from "class-validator";

export interface ClassValidatorDefinition {
	validate: any;
}

export class ClassValidator {
	@Allow()
	validate: any;
}

ClassValidator.prototype.validate = function () {
	return doValidate(this);
};

export const ClassValidatorModel = (model: any, validatorClass: any) => {
	model.prototype.validate = function () {
		return new validatorClass(this).validate();
	};

	return model;
};
