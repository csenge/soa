import * as Knex from "knex";

const getKnexConfiguration = (
	clientType: string,
	dbIdentifierPrefix: string,
) => {
	return {
		client: clientType,
		connection: {
			host: process.env[`${dbIdentifierPrefix}_HOST`],
			user: process.env[`${dbIdentifierPrefix}_USER`],
			password: process.env[`${dbIdentifierPrefix}_PASSWORD`],
			database: process.env[`${dbIdentifierPrefix}_NAME`],
			timezone: "UTC",
		},
		pool: {
			min: 0,
			max: 10,
		},
	};
};

export const createKnexConnection = async (
	clientType: string,
	dbIdentifierPrefix: string,
) => {
	return async (callback: any) => {
		const knexClient = createKnexInstance(clientType, dbIdentifierPrefix);

		try {
			return await callback(knexClient);
		} finally {
			await knexClient.destroy();
		}
	};
};

export const createKnexInstance = (
	clientType: string,
	dbIdentifierPrefix: string,
) => {
	return Knex(getKnexConfiguration(clientType, dbIdentifierPrefix));
};

export const createKnexTransaction = async (knex: Knex, callback: any) => {
	return await knex.transaction(callback);
};

export const dbExecute = async (dbType: string, dbIdentifierPrefix: string, callback: any) => {
	const knexConnection = await createKnexInstance(dbType, dbIdentifierPrefix);

	const response = await createKnexTransaction(knexConnection, callback);

	knexConnection.destroy();

	return response;
};

export const mysqlDbExecute = async (dbIdentifierPrefix: string, callback: any) => {
	return await dbExecute('mysql', dbIdentifierPrefix, callback);
};

export const createMysqlKnexInstance = (dbIdentifierPrefix: string) => {
	return Knex(getKnexConfiguration('mysql', dbIdentifierPrefix));
};
