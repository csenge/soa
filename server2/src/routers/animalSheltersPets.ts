import express from "express";
import * as AnimalShelterPetsController from "../controllers/PetsController";
import handleAsyncRoutes from "../middlewares/handleAsyncRoutes";

export const handler = function(app: express.Application): express.Application  {
	app.get("/animalShelters/:animalShelterId/pets", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				animalShelterId,
			},
		} = req;

		return await AnimalShelterPetsController.getAll(animalShelterId);
	}));

	app.get("/animalShelters/:animalShelterId/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				id,
			},
		} = req;

		return await AnimalShelterPetsController.getById(id);
	}));

	app.post("/animalShelters/:animalShelterId/pets/filter", handleAsyncRoutes(async (req: express.Request) => {
		const {
			body: {
				pagination,
				filters,
				sorting,
			},
			params: {
				animalShelterId,
			},
		} = req;

		return await AnimalShelterPetsController.getFilteredData(
			pagination,
			filters,
			sorting,
			animalShelterId,
		);
	}));

	app.post("/animalShelters/:animalShelterId/pets", handleAsyncRoutes(async (req: express.Request) => {
		const {
			body,
			params: {
				animalShelterId,
			},
		} = req;

		return await AnimalShelterPetsController.add(body, animalShelterId);
	}));

	app.put("/animalShelters/:animalShelterId/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			body,
			params: {
				id,
			},
		} = req;

		return await AnimalShelterPetsController.update(id, body);
	}));

	app.patch("/animalShelters/:animalShelterId/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			body,
			params: {
				id,
			},
		} = req;

		return await AnimalShelterPetsController.patch(id, body);
	}));

	app.delete("/animalShelters/:animalShelterId/pets/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				id,
			},
		} = req;

		return await AnimalShelterPetsController.remove(id);
	}));

	return app;
};
