import express from "express";
import * as AnimalSheltersController from "../controllers/AnimalSheltersController";
import handleAsyncRoutes from "../middlewares/handleAsyncRoutes";

export const handler = function(app: express.Application): express.Application  {
	app.get("/animalShelters", handleAsyncRoutes(async () => {
		return await AnimalSheltersController.getAll();
	}));

	app.get("/animalShelters/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				id,
			},
		} = req;

		return await AnimalSheltersController.getById(id);
	}));

	app.post("/animalShelters/filter", handleAsyncRoutes(async (req: express.Request) => {
		const {
			body: {
				pagination,
				filters,
				sorting,
			},
		} = req;

		return await AnimalSheltersController.getFilteredData(
			pagination,
			filters,
			sorting,
		);
	}));

	app.post("/animalShelters", handleAsyncRoutes(async (req: express.Request) => {
		const { body } = req;

		return await AnimalSheltersController.add(body);
	}));

	app.put("/animalShelters/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params,
			body,
		} = req;

		return await AnimalSheltersController.update(params.id, body);
	}));

	app.patch("/animalShelters/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params,
			body,
		} = req;

		return await AnimalSheltersController.patch(params.id, body);
	}));

	app.delete("/animalShelters/:id", handleAsyncRoutes(async (req: express.Request) => {
		const {
			params: {
				id,
			},
		} = req;

		return await AnimalSheltersController.remove(id);
	}));

	return app;
};
