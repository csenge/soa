import {
	Allow,
	MinLength,
} from "class-validator";
import {
	ClassValidatorDefinition,
	ClassValidator,
} from "../helpers/classValidator/ClassValidatorModel";

export interface PetDefinition extends ClassValidatorDefinition {
	id?: number;
	petTypeId: number;
	animalShelterId: number;
	name: string;
	age: number;
	isAdopted: boolean;
	details?: any;
}

export class PetModel extends ClassValidator {
	@Allow()
	id?: string | number;

	@MinLength(1)
	petTypeId: number;

	@MinLength(1)
	animalShelterId: number;

	@MinLength(1)
	name: string;

	@MinLength(1)
	age: number;

	@Allow()
	isAdopted: boolean;

	@Allow()
	details: string;

	constructor(pet: PetDefinition) {
		super();

		this.id = pet.id;
		this.petTypeId = pet.petTypeId;
		this.animalShelterId = pet.animalShelterId;
		this.name = pet.name;
		this.age = pet.age;
		this.isAdopted = pet.isAdopted;
		this.details = pet.details;
	}
}
