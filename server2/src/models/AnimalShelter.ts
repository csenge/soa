import {
	Allow,
	MinLength,
} from "class-validator";
import {
	ClassValidatorDefinition,
	ClassValidator,
} from "../helpers/classValidator/ClassValidatorModel";

export interface AnimalShelterDefinition extends ClassValidatorDefinition {
	id?: number;
	name: string;
	location: string;
	description: string;
	picture: string;
}

export class AnimalShelterModel extends ClassValidator {
	@Allow()
	id?: number;

	@MinLength(1)
	name: string;

	@MinLength(1)
	location: string;

	@MinLength(1)
	description: string;

	@MinLength(1)
	picture: string;

	constructor(animalShelter: AnimalShelterDefinition) {
		super();

		this.id = animalShelter.id;
		this.name = animalShelter.name;
		this.location = animalShelter.location;
		this.description = animalShelter.description;
		this.picture = animalShelter.picture;
	}
}
