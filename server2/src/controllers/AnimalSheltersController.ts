import * as AnimalSheltersDao from "../dao/AnimalSheltersDao";
import { AnimalShelterDefinition, AnimalShelterModel } from "../models/AnimalShelter";
import ErrorHandler from '../utils/ErrorHandler';
import {
	GenericFilters,
	GenericOrder,
	GenericPagination,
} from "../typesX";

export const getAll = async () => {
	return await AnimalSheltersDao.getAll();
};

export const getFilteredData = async (
	paginationRules?: GenericPagination,
	filterRules?: GenericFilters,
	orderRules?: GenericOrder,
) => {
	return await AnimalSheltersDao.getFiltered(paginationRules, filterRules, orderRules);
};

export const getById = async (id: AnimalShelterDefinition["id"]) => {
	if (!id) {
		throw ErrorHandler.badRequest("ANIMAL_SHELTER_ID_NOT_PROVIDED");
	}

	const animalShelter = await AnimalSheltersDao.getById(id);

	if (!animalShelter) {
		throw ErrorHandler.notFound("ANIMAL_SHELTER_NOT_FOUND");
	}

	return animalShelter;
};

export const add = async (animalShelterData: AnimalShelterDefinition) => {
	const animalShelter = new AnimalShelterModel(animalShelterData);

	const errors = await animalShelter.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const createdUser = AnimalSheltersDao.add(animalShelter);

	if (!createdUser) {
		throw ErrorHandler.badImplementation("ANIMAL_SHELTER_CANNOT_CREATE");
	}

	return createdUser;
};

export const remove = async (id: AnimalShelterDefinition["id"]) => {
	if (!id) {
		throw ErrorHandler.badRequest("ANIMAL_SHELTER_ID_NOT_PROVIDED");
	}

	const removed = await AnimalSheltersDao.remove(id);

	if (!removed) {
		throw ErrorHandler.badImplementation("ANIMAL_SHELTER_CANNOT_REMOVE");
	}

	return {
		removed,
	};
};

export const update = async (id: AnimalShelterDefinition["id"], animalShelterData: AnimalShelterDefinition) => {
	const animalShelter = new AnimalShelterModel(animalShelterData);

	if (!id) {
		throw ErrorHandler.badRequest("ANIMAL_SHELTER_ID_NOT_FOUND");
	}

	const errors = await animalShelter.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const updatedUser = await AnimalSheltersDao.update(id, animalShelter);

	if (!updatedUser) {
		throw ErrorHandler.badImplementation("ANIMAL_SHELTER_CANNOT_UPDATE");
	}

	return updatedUser;
};

export const patch = async (id: AnimalShelterDefinition["id"], data: any) => {
	if (!id) {
		throw ErrorHandler.badRequest("ANIMAL_SHELTER_ID_NOT_FOUND");
	}

	const oldUser: any = await getById(id);
	const newUser = new AnimalShelterModel(Object.assign({}, oldUser, data));

	const errors = await newUser.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const updatedUser = await AnimalSheltersDao.update(id, newUser);

	if (!updatedUser) {
		throw ErrorHandler.badImplementation("ANIMAL_SHELTER_CANNOT_UPDATE");
	}

	return updatedUser;
};
