import * as request from "request-promise";
import * as PetsDao from "../dao/PetsDao";
import { PetDefinition, PetModel } from "../models/Pet";
import { AnimalShelterDefinition } from "../models/AnimalShelter";
import ErrorHandler from '../utils/ErrorHandler';
import {
	GenericFilters,
	GenericOrder,
	GenericPagination,
} from "../typesX";

const getPetsDetailsData = async (petTypeId: number) => {
	const dataDetails = await request(`${process.env.PETS_LIBRARY_HOST}/pets/${petTypeId}`);

	if (dataDetails) {
		try {
			const details = JSON.parse(dataDetails);

			return details.data;
		} catch (e) {
			console.error('Failde to convert to json');
			return null;
		}
	}
};

export const getAll = async (animalShelterId ?: AnimalShelterDefinition["id"]) => {
	const pets: any = await PetsDao.getAll(animalShelterId);

	for (let i = 0; i < pets.length; i++) {
		const pet = pets[i];

		pet.details = await getPetsDetailsData(pet.petTypeId);
	}

	return pets;
};

export const getFilteredData = async (
	paginationRules?: GenericPagination,
	filterRules?: GenericFilters,
	orderRules?: GenericOrder,
	animalShelterId ?: AnimalShelterDefinition["id"],
) => {
	const pets: any = PetsDao.getFiltered(paginationRules, filterRules, orderRules, animalShelterId);

	for (let i = 0; i < pets.length; i++) {
		const pet = pets[i];

		pet.details = await getPetsDetailsData(pet.petTypeId);
	}

	return pets;
};

export const getById = async (id: PetDefinition["id"]) => {
	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_PROVIDED");
	}

	const pet: any = await PetsDao.getById(id);

	console.log('&&&&&');
	console.log(pet);

	if (pet) {
		console.log('^^^^', pet.petTypeId);
		pet.details = await getPetsDetailsData(pet.petTypeId);
	}

	if (!pet) {
		throw ErrorHandler.notFound("PET_NOT_FOUND");
	}

	console.log(pet);

	return pet;
};

export const add = async (petData: PetDefinition, animalShelterId ?: AnimalShelterDefinition["id"]) => {
	const pet = new PetModel(petData);

	const errors = await pet.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	if (animalShelterId) {
		pet.animalShelterId = animalShelterId;
	}

	const createdUser = PetsDao.add(pet);

	if (!createdUser) {
		throw ErrorHandler.badImplementation("PET_CANNOT_CREATE");
	}

	return createdUser;
};

export const remove = async (id: PetDefinition["id"]) => {
	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_PROVIDED");
	}

	const removed = await PetsDao.remove(id);

	if (!removed) {
		throw ErrorHandler.badImplementation("PET_CANNOT_REMOVE");
	}

	return {
		removed,
	};
};

export const update = async (id: PetDefinition["id"], petData: PetDefinition) => {
	const pet = new PetModel(petData);

	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_FOUND");
	}

	const errors = await pet.validate();

	if (errors && errors.length) {
		throw ErrorHandler.badData(JSON.stringify(errors));
	}

	const updatedUser = await PetsDao.update(id, pet);

	if (!updatedUser) {
		throw ErrorHandler.badImplementation("PET_CANNOT_UPDATE");
	}

	return updatedUser;
};

export const patch = async (id: PetDefinition["id"], data: any) => {
	if (!id) {
		throw ErrorHandler.badRequest("PET_ID_NOT_FOUND");
	}

	const updatedUser = await PetsDao.update(id, data);

	if (!updatedUser) {
		throw ErrorHandler.badImplementation("PET_CANNOT_UPDATE");
	}

	return updatedUser;
};
