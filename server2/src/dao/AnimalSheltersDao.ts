import * as Knex from "knex";
import { AnimalShelterDefinition, AnimalShelterModel } from "../models/AnimalShelter";
import modelNormalizer from "../utils/modelNormalizer";
import { applyGenericFilters } from "../helpers/knex/applyGenericFilters";
import { mysqlDbExecute as dbExecute } from "../helpers/knex/client";
import {
	GenericFilters,
	GenericOrder,
	GenericPagination,
} from "../typesX";

const TABLE_NAME = "animal_shelters";
const DB_SECRETS_PREFIX = "MYSQL_DB";

export const getAll = async () => {
	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		const animalShelters = await trx(TABLE_NAME).select();

		return modelNormalizer(animalShelters, AnimalShelterModel);
	});
};

export const getById = async (id: AnimalShelterDefinition["id"]) => {
	if (!id) {
		return null;
	}

	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		const animalShelters = await trx(TABLE_NAME).select().where("id", id);

		if (!animalShelters || !animalShelters.length) {
			return null;
		}

		return modelNormalizer(animalShelters, AnimalShelterModel);
	});
};

export const getFiltered = async (
	paginationRules?: GenericPagination,
	filterRules?: GenericFilters,
	orderRules?: GenericOrder,
) => {
	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		let query = trx(TABLE_NAME);

		if (filterRules || orderRules || paginationRules) {
			query = applyGenericFilters(query, paginationRules, filterRules, orderRules);
		}

		const animalShelters = await query.select();

		return modelNormalizer(animalShelters, AnimalShelterModel);
	});
};

export const add = async (animalShelters: AnimalShelterDefinition) => {
	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		const result = await trx(TABLE_NAME).insert(animalShelters);

		if (!result || result.length !== 1) {
			return null;
		}

		return result[0];
	}).then(async (id: any) => {
		if (!id) {
			const documents: any = await getById(id);

			return documents[0];
		}
	});
};

export const remove = async (id: AnimalShelterDefinition["id"]) => {
	if (!id) {
		return null;
	}

	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		return await trx(TABLE_NAME).delete().where("id", id);
	}).then((removed: any) => {
		return !!removed;
	});
};

export const update = async (id: AnimalShelterDefinition["id"], animalShelters: AnimalShelterDefinition) => {
	if (!id) {
		return null;
	}

	return dbExecute(DB_SECRETS_PREFIX, async (trx: Knex) => {
		return await trx(TABLE_NAME).update(animalShelters).where("id", id);
	}).then(async () => await getById(id));
};
