export interface GenericFilter {
	property: string;
	value: any;
	op?: string;
}

export interface GenericOrder {
	[key: string]: OrderDirections;
}

export interface GenericPagination {
	limit: number;
	lastEvaluatedObject?: string;
	offset?: number;
}

export type OrderDirections = "asc" | "desc";

// @ts-ignore
export type GenericFilters = Array<GenericFilter | GenericFilters>;
