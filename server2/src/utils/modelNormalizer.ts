export default function(data: any, model: any) {
	return data.map((item: any) => {
		return new model(item);
	});
}
