import express from "express";

export default function(fn: any) {
	return async (req: express.Request, res: express.Response, next: express.NextFunction) => {
		try {
			const data = await fn(req, res, next);

			res.status(200);
			res.json({
				success: true,
				data,
			});
		} catch (err) {
			next(err);
		}
	};
}
