import * as express from "express";

export default function(
	err: any,
	_req: express.Request,
	res: express.Response,
	next: express.NextFunction,
) {
	if (res.headersSent) {
		return next(err);
	}

	if (err.output && err.output.statusCode) {
		res.status(err.output.statusCode);
	} else {
		res.status(500);
	}

	res.json({
		error: {
			message: err.message,
		},
	});
}
